package com.twitter;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    private static BufferedReader bufferedReader;
    private static PrintWriter printWriter;

    //Data structure to keep track of the count of hash tags in incoming tweets
    static Map<String, Integer> hashTagCountMap = new HashMap<>();

    public static void main(String[] args) throws IOException {
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        printWriter = new PrintWriter(new OutputStreamWriter(System.out));
        int noOfTweets = Integer.parseInt(bufferedReader.readLine());

        for (int i = 0; i < noOfTweets; i++) {
            String tweet = checkTweetCharacterCountLimit(bufferedReader.readLine());
           extractHashTag(tweet);
        }
        trendingHashTags(hashTagCountMap);
        bufferedReader.close();
        printWriter.close();
    }

    protected static String checkTweetCharacterCountLimit(String tweet) {
        if (tweet.length() > 280) {
            throw new TweetCharacterLimitExceededException("Character count for this tweet exceed");
        }
        return tweet;
    }

    //Method to extract Hastag from each tweet
    protected static void extractHashTag(String tweet) {
        Pattern REGEX_HASH_TAG_PATTERN = Pattern.compile("#(\\S+)"); //Regular expression to find characters after Hashtag in a string
        Matcher matcher = REGEX_HASH_TAG_PATTERN.matcher(tweet);
        while (matcher.find()) {
            String hashTag = matcher.group(1);
            hashTagCountMap.put(hashTag, hashTagCountMap.getOrDefault(hashTag, 0) + 1);// Adding hastags to a map and increasing count each time if it already exisits
        }

    }

    //Method to compare count of hastags in hashTagCountMap
    protected static void trendingHashTags(Map<String,Integer> hashTagCountMap ) {
        Entry<String, Integer> trendingHashTags[] = hashTagCountMap.entrySet().toArray(new Entry[0]);//Contains sorted hashtags map with highest values and in alphabetical order
        Arrays.sort(trendingHashTags, new Comparator<Entry<String, Integer>>() {
            public int compare(Entry<String, Integer> value1, Entry<String, Integer> value2) {
                if (value1.getValue().equals(value2.getValue())) // If the count of two hashtags are equal then we compare keys in alphabetical order
                    return value1.getKey().compareTo(value2.getKey());
                return value2.getValue().compareTo(value1.getValue());
            }
        });

        //printing top 10 trending Hashmaps
        for (int i = 0; i < Math.min(trendingHashTags.length, 10); i++) {
            Entry<String, Integer> it = trendingHashTags[i];
            printWriter.println("#" + it.getKey());
        }
    }


}
